//profile.html
$(function() {
  $("#accordion").accordion({
    collapsible: true
  });
});

$(function() {
  $("div[name='content']").hide().delay(2500).slideDown(2000);
  $("#landing").delay(2500).slideUp(900);
});

$( function() {
  var color = ["#d25557", "#9688B4"];
  var i = 0;
  $("#button").click(function() {
    i = (i + 1) % 2;
    $("body").css('background', color[i]);
    $(".button1").css('background-color', color[(i + 1) % 2])
  });
});

$( function() {
  var color = ["#A1282A", "#3D4258"];
  var i = 0;
  $("#button").click(function() {
    i = (i + 1) % 2;
    $("#header").css('background-color', color[i]);
  });
});

$("#loading-bar").each(function(i) {
  var delay = 2000;
  $(this).delay(delay * i).animate({
    width: $(this).attr('aria-valuenow') + '%'
  }, delay);
});
// profile.html

// books.html
var count = 0;
function key(param) {
  $.ajax( {
    type: 'GET',
    url: "/dataJson/"+param
  }).done(function(data) {
    count = 0;
    $('#body').html('');
    $('#count').text('0');
    var lines = $('#body');
    var counter = 0;
    $(data.items).each(function(i, x) {
      // counter plus 1 for each data
      counter = counter + 1;
      $(x.volumeInfo).each(function(j, k) {
        // get the authors and title for each data
        var author = k.authors;
        if (author == null) {
          author = "(name is undefined)";
        }
        var title = k.title;
        if (title == null) {
          title = "(title is undefined)";
        }
        $(k.imageLinks).each(function(l, m) {
          // Make a base code for new row for table
          var newline = $('<tr></tr>');
          // Make a base code for book cover in column 1 in row
          var image = $('<td></td>');
          // Make a base code for author name in colums 2 in row
          var athr = $('<td style="vertical-align: middle !important; text-align: center; font-size: 16px;"></td>');
          // Make a base code for title of book in colums 3 in row
          var ttl = $('<td style="vertical-align: middle !important; text-align: center; font-size: 15px;"></td>');
          // Make a base code for favorite button in colums 4 in row
          var button = $('<td style="vertical-align: middle !important;"><i class="fa fa-star" style="font-size: 75px; color: rgb(128, 128, 128);" id="btn'+counter+'"></i></td>')
          // Make a base code for image shown in colums 1 in row
          var img = $('<img></img>');
          // add source of image thumbnail & append it to column 1
          img.attr('src', m.thumbnail);
          image.append(img);
          // append author and title in column 2 ans column 3
          athr.append(author);
          ttl.append(title);
          // make a new line in table
          newline.append(image);
          newline.append(athr);
          newline.append(ttl);
          newline.append(button);
          // append the new line to the table
          lines.append(newline);
        });
      });
    });
  });
}

$(function() {
  var color = ["rgb(128, 128, 128)", "rgb(161, 40, 42)"];
  $('body').on('click', 'i', function() {
    var $a = $(this).css('color');
    if ($a == color[0]) {
      $(this).css('color', color[1]);
      count++;
    }
    else if ($a == color[1]) {
      $(this).css('color', color[0]);
      count--;
    }
    $('#count').text(count);
  });
});
// books.html

//regis.html
$(function() {
  var email_is_okay = false;
  var username_is_okay = false;
  var password_is_okay = false;
  var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
  
  button_switch('off');

  $('body').on('blur', '#register-username', function() {
    var username = $('#register-username').val();
    console.log(username);
    if(username == ''){
          $('#alert-username').text('username cannot be blank or empty !');
          $('#span-username').css('background-color', '#cc0000');
          username_is_okay = false;
          all_is_okay()
      }
      else {
        $('#alert-username').text('');
        $('#span-username').css('background-color', '#00cc00');
        username_is_okay = true;
        all_is_okay()
      }
  });

  $('body').on('blur', '#register-email', function() {
    var email_input_value = document.getElementById("register-email").value;
    console.log(email_input_value);
    var flag = validate();
    $.ajax( {
      url: "/registration/"+email_input_value
    }).done(function(data) {
      console.log(flag);
      if (flag == false) {
        $('#alert-email').text("fill the email field or correct the input (must be email type) !");
        $('#span-email').css('background-color', '#cc0000');
        email_is_okay = false;
        all_is_okay();
      }
      else if (data.status) {
        $('#alert-email').text(data.message);
        $('#span-email').css('background-color', '#cc0000');
        email_is_okay = false;
        all_is_okay();
      }
      else if (!data.status) {
        if (flag == true) {}
          $('#alert-email').text("");
          $('#span-email').css('background-color', '#00cc00');
          email_is_okay = true;
          all_is_okay();
      }
    });
  });

  $('body').on('blur', '#register-password',function() {
    var password = $('#register-password').val();
    console.log(password)
    if(password == ''){
          $('#alert-password').text('password cannot be blank or empty !');
          $('#span-password').css('background-color', '#cc0000');
          password_is_okay = false;
          all_is_okay()
      }
      else {
        $('#alert-password').text('');
        $('#span-password').css('background-color', '#00cc00');
        password_is_okay = true;
        all_is_okay()
      }
  });

  $('form').on('submit', function(e) {
    e.preventDefault();
    var email = $('#register-email').val();
    console.log(email);
    var username = $('#register-username').val();
    var password = $('#register-password').val();
    var data = {
              'email': email,
              'username': username,
              'password': password,
        };
    $.ajax( {
      url: '/addSubscriber/',
      type: 'POST',
      data: data,
      success: function (result) {
        $('#register-email').val('');
        $('#register-username').val('');
        $('#register-password').val('');
        $('#submit-status').hide(100).show(100).text(result.message);
        $('#submit-status').css('color', '#00cc00');
        $('#span-email').css('background-color', '#ccc');
        $('#span-username').css('background-color', '#ccc');
        $('#span-password').css('background-color', '#ccc');
        button_switch('off');
      },
      error: function(result) {
        $('#submit-status').text(result.message);
        $('#submit-status').css('color', '#cc0000');
      }
    });
  });

  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader('X-CSRFToken', csrftoken);
      }
    }
  });
});

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
    var email = document.getElementById("register-email").value;

    if (validateEmail(email)) {
      return true;
    } else {
      return false;
    }
    return false;
}

function button_switch(side) {
  if (side == 'on') {
    $('#btn-register').attr('disabled', false);
  }
  else if (side = 'off') {
    $('#btn-register').attr('disabled', true);
  }
    
}

function all_is_okay() {
  if (email_is_okay && username_is_okay && password_is_okay) {
    button_switch('on');
  }
  else {
    button_switch('off');
  }
}

$(function() {
  
});

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
//regis.html