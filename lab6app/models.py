from django.db import models
from django.utils.timezone import now

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300)
    datetime = models.DateTimeField(default=now, blank=True)

class Register(models.Model):
	email = models.EmailField(max_length=50, blank=False)
	username = models.CharField(max_length=32, blank=False)
	password = models.CharField(max_length=32, blank=False)
