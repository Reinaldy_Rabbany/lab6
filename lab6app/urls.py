from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('', delete, name='delete'),
    path('profile/', profile, name='profile'),
    path('books/', books, name='books'),
    path('dataJson/<searchs>/', dataJson, name='dataJson'),
    path('registration/', registration, name='registration'),
    path('registration/<email>', validate, name='validate'),
    path('addSubscriber/', subscribe, name='subscribe'),
    path('getSubscriber/', get_subscriber, name='getSubscriber'),
    path('unsubscribe/', unsubscribe, name='unsubscribe')
]
