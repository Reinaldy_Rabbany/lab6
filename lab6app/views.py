from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from lab6app.models import Status, Register
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from . import forms
import requests
import json

# Create your views here.
def index(request):
    if request.method == "POST":
        form = forms.FormStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = forms.FormStatus()

    status_list = Status.objects.all().order_by('-datetime')
    response = {
        'form' : form, 
        'status' : status_list
    }
    return render(request, 'index.html', response)

def profile(request):
    return render(request, "profile.html")

def delete(request):
	status = Status.objects.all().delete()
	return render(request, 'index.html', {'status' : status})

def books(request):
    return render(request, "books.html")

def dataJson(request, searchs=""):
    URL = "https://www.googleapis.com/books/v1/volumes?q="+searchs
    req = requests.get(url = URL)
    json = req.json()
    return JsonResponse(json)

def validate(request, email=""):
    email_status = Register.objects.filter(email__iexact=email).exists()
    status = {'status': email_status}
    if status['status']:
        status['message'] = 'This email has already been registered before, please register with another email.'
    else:
        status['message'] = 'This email has not been registered and can be used'

    json_data = json.dumps(status)
    return HttpResponse(json_data, content_type='application/json')

def registration(request):
    return render(request, "regis.html")

@csrf_exempt    
def subscribe(request):
    response_data = {}
    if request.method == 'POST':
        data = forms.FormRegister(request.POST)
        if data.is_valid():
            data.save()
            response_data['status'] = True
            response_data['message'] = 'All your data has been successfully saved'
            json_data = json.dumps(response_data)
            return HttpResponse(json_data, content_type='application/json')
        else:
            response_data['status'] = False
            response_data['message'] = 'Fail to save the data'
            json_data = json.dumps(response_data)
            return HttpResponse(json_data, content_type='application/json')

def get_subscriber(request):
    response = {}
    subscribers = Register.objects.all()
    response = [{'id':subs.id, 'email':subs.email, 'username':subs.username} for subs in subscribers]
    return HttpResponse(json.dumps({'subscribers': response}),
                        content_type='application/json')

@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        subscriber_id = request.POST['subscriber_id']
        Register.objects.get(id=subscriber_id).delete()
        return HttpResponse(json.dumps({'message': 'unsubscribe success!'}), content_type='application/json')
    else:
        HttpResponse(json.dumps({'message': 'unsubscribe unsuccess!'}), content_type='application/json')
