from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils.timezone import now
from .views import *
from .forms import FormStatus
from .models import Status

import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
DRIVER_BIN = os.path.join(PROJECT_ROOT, "/chromedriver")

# Create your tests here.
class VisitorTest(TestCase):
    def test_if_hey_exist(self):
        test = "Hey There !"
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_if_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_url_not_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_form_validation_for_blank_items(self):
        form = FormStatus(data={'status' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_models_if_on_the_database(self):
        new_activity = Status.objects.create(status='lagi ngerjain story ppw nih!', datetime=now())

        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_if_post_success_and_render_the_result(self):
        test = 'Biasa aja'
        response_post = Client().post('/', {'status' : test, 'datetime' : now})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_post_failed_and_render_the_result(self):
        test = 'Biasa aja'
        response_post = Client().post('/', {'status' : '', 'datetime' : ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_if_myname_exist(self):
        test = "Reinaldy Rabbany"
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_profile_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_profile_not_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_if_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

# class LayoutAndStylingTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')        
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(AccountTestCase, self).tearDown()

#     def test_the_title(self):
#         selenium = self.selenium
#         selenium.get('http://myportfolioweb-ppw.herokuapp.com/')
#         title = selenium.title
#         self.assertEqual('Home', title)

#     def test_the_content_first_line():
#         selenium = self.selenium
#         selenium.get('http://myportfolioweb-ppw.herokuapp.com/')
#         self.assertIn('Halo, Nama Saya Reinaldy Rabbany', selenium.page_source)

#     def test_css_font_p_white_color():
#         selenium = self.selenium
#         selenium.get('http://myportfolioweb-ppw.herokuapp.com/')

#         elem = selenium.find_element_by_id('p-white')
#         value = elem.value_of_css_property('color')

#         self.assertEqual('white', value)

#     def test_css_font_h2_black_color():
#         selenium = self.selenium
#         selenium.get('http://myportfolioweb-ppw.herokuapp.com/')

#         elem = selenium.find_element_by_id('h2-black')
#         value = elem.value_of_css_property('color')

#         self.assertEqual('black', value)

class FunctionalTest(TestCase):
    def setUp(self):
        # browser = webdriver.Chrome(executable_path = DRIVER_BIN)
        super(FunctionalTest, self).setUp()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        # Open the link
        self.selenium.get('http://localhost:8000/')
        
        # init the form element
        status = self.selenium.find_element_by_id('status')
        submit = self.selenium.find_element_by_id('submit-status')
        
        # Fill the form
        status.send_keys('Coba Coba')

        # submit the form
        submit.send_keys(Keys.RETURN)

        # check if "Coba Coba" is in the page
        self.assertIn('Coba Coba', self.selenium.page_source)


#idea from: Laksono Bramantio
