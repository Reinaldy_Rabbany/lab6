from django.apps import AppConfig


class Lab6AppConfig(AppConfig):
    name = 'lab6app'
