from django import forms
from .models import Status, Register

class FormStatus(forms.ModelForm):
    status = forms.CharField(max_length=300)

    class Meta:
        model = Status
        fields = ('status', )

class FormRegister(forms.ModelForm):
	email = forms.EmailField(max_length=50, required=True)
	username = forms.CharField(max_length=32, required=True)
	password = forms.CharField(max_length=32, required=True, widget=forms.PasswordInput())
	
	class Meta:
		model = Register
		fields = ('email', 'username', 'password', )
		